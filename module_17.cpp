#include <iostream>
#include <cmath>

class Vector
{

private:

	double x = 0;
    double y = 0;
	double z = 0;

public:

	Vector()
	{}

	Vector(int _x, int _y,int _z) : x(_x),y(_y),z(_z)
	{}

	void Show()
	{
		std::cout << x << ' ' << y << ' ' << z << "\n";
	}

	void Length()
	{
		std::cout << sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}
};


int main()
{
	Vector vec(7,-5,3);

	vec.Show();

	vec.Length();
}


